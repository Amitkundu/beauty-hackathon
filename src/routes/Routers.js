import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import Home from "../pages/Home.js";
import AllFoods from "../pages/AllFoods.js";
import FoodDetails from "../pages/FoodDetails.js";
import Cart from "../pages/Cart.js";
import Checkout from "../pages/Checkout.js";
import Contact from "../pages/Contact.js";


const Routers = () => {
  return (
    <Routes>
     
      <Route exact path="/" element={<Home />} />
      <Route exact path="/products" element={<AllFoods />} />
      <Route exact path="/products/:id" element={<FoodDetails />} />
      <Route path="/cart" element={<Cart />} />
      <Route path="/checkout" element={<Checkout />} />
      {/*<Route path="/login" element={<Login />} />*/}
      {/*<Route path="/register" element={<Register />} />*/}
      {/*<Route path="/contact" element={<Contact />} />*/}
    </Routes>
  );
};

export default Routers;
