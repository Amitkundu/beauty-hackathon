import React from "react";
import { Container, Row, Col, ListGroup, ListGroupItem } from "reactstrap";
import logo from "../../assets/images/res-logo.png";
import "../../styles/footer.css";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <footer className="footer">
    <h2 className="a11y">Footer</h2>
      <Container>
        <Row>
          <Col lg="3" md="4" sm="6">
            <div className=" footer__logo text-start">
              <img src={logo} alt="Beautify logo" />
              <h3>Beautify</h3>
              <p>
                Beautiful skin starts here
              </p>
            </div>
          </Col>

          <Col lg="3" md="4" sm="6">
            <h3 className="footer__title">Delivery Time</h3>
            <ListGroup className="deliver__time-list">
              <ListGroupItem className=" delivery__time-item border-0 ps-0">
                <span>Sunday - Friday</span>
                <p>10:00am - 11:00pm</p>
              </ListGroupItem>

              <ListGroupItem className=" delivery__time-item border-0 ps-0">
                <span>Saturday</span>
                <p>Off day</p>
              </ListGroupItem>
            </ListGroup>
          </Col>

          <Col lg="3" md="4" sm="6">
            <h3 className="footer__title">Contact</h3>
            <ListGroup className="deliver__time-list">
              <ListGroupItem className=" delivery__time-item border-0 ps-0">
                <p>Location: Mumbai Dadar, Bandra, Andheri. Churchgate</p>
              </ListGroupItem>
            </ListGroup>
          </Col>

          <Col lg="3" md="4" sm="6">
            <ListGroup className="deliver__time-list pt-4">
              <ListGroupItem className=" delivery__time-item border-0 ps-0">
                <span>Phone: xxx xxx xx xx</span>
              </ListGroupItem>

              <ListGroupItem className=" delivery__time-item border-0 ps-0">
                <span>Email: xxx@xxx.com</span>
              </ListGroupItem>
            </ListGroup>
          </Col>
        </Row>

        <Row className="mt-5">
          <Col lg="12" md="12">
            <p className="fw-bold copyright__text">
              Copyright - 2023, Beautify. All Rights
              Reserved.
            </p>
          </Col>
          
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;
