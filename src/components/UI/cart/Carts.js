import React, {useEffect, useRef, useState} from "react";
import { ListGroup } from "reactstrap";
import { Link } from "react-router-dom";
import CartItem from "./CartItem";
import { useDispatch, useSelector } from "react-redux";
import { cartUiActions } from "../../../store/shopping-cart/cartUiSlice";
import "../../../styles/shopping-cart.css";
import { handleFocus,handleDialog, reStoreTabIndex } from "../../../utils/util.js";

const Carts = () => {
  const [removeIndex, setRemoveIndex] = useState(-1);
  const dispatch = useDispatch();
  const cartProducts = useSelector((state) => state.cart.cartItems);
  const totalAmount = useSelector((state) => state.cart.totalAmount);
  const dialogRef = useRef(null);

  const toggleCart = () => {
    dispatch(cartUiActions.toggle());
  };

  useEffect(() => {
    if(dialogRef && dialogRef.current) {
      dialogRef.current.focus();
      handleDialog();
    }
  }, []);

  const onDeleteIndex = (indx) => {
    setRemoveIndex(indx);
  }

  const handleClose = () => {
    toggleCart();
    reStoreTabIndex();
    setTimeout(() => {
      const triggerBtn = document.querySelector("#triggerBtn");
      if(triggerBtn) triggerBtn.focus();
    }, 350);
  }

  useEffect(() => {
    if (removeIndex !==-1) {
      handleFocus(removeIndex);
      setRemoveIndex(-1);
    }
  }, [cartProducts]);

  return (
    <div id="dialog" ref={dialogRef} tabIndex="-1" className="cart__container" role="dialog" aria-modal="true" aria-label="Shopping cart">
      <ListGroup className="cart">
        <div className="cart__close">
          <button id="closeDialog" className="ax-button" onClick={handleClose} aria-label="Close Dialog">
            <i class="ri-close-fill"></i>
          </button>
        </div>

        <div className="cart__item-list">
          {cartProducts.length === 0 ? (
            <h3 id="no_item" className="text-center mt-5" tabIndex="-1">No item added to the cart</h3>
          ) : (
            cartProducts.map((item, index) => (
              <CartItem key={index} item={item} index={index}  deleteIndex={(indx) => onDeleteIndex(indx)} />
            ))
          )}
        </div>

        <div className="cart__bottom d-flex align-items-center justify-content-between">
          <h3>
            Subtotal : <span>${totalAmount}</span>
          </h3>
          <span className={`checkout ${totalAmount === 0 ? 'disabled' : ''}`}>
            <Link  tabIndex={totalAmount === 0 ? "-1" : "0"} aria-disabled={totalAmount === 0 ? "true" : "false"} to="/checkout" onClick={toggleCart}>
              Checkout
            </Link>
          </span>
        </div>
      </ListGroup>
    </div>
  );
};

export default Carts;
