import React, {useState} from "react";
import { ListGroupItem } from "reactstrap";
import "../../../styles/cart-item.css";
import { useDispatch } from "react-redux";
import { cartActions } from "../../../store/shopping-cart/cartSlice";
import CustomizedSnackbars from "../../CustomizedSnackbars";

const CartItem = ({ item , index = 0 ,deleteIndex}) => {
  const { id, title, price, image01, quantity, totalPrice } = item;
  const [isShowAlert, showAlert] = useState(false);
  const [text, setText] = useState("Item added to Cart");

  const dispatch = useDispatch();

  const incrementItem = () => {
    const titleText = `Added ${title} to cart by 1`;
    showAlert(false);
    setText(titleText);
    showAlert(true);

    dispatch(
      cartActions.addItem({
        id,
        title,
        price,
        image01,
      })
    );
  };

  const decreaseItem = () => {
    const titleText = `Decrease ${title} from cart by 1`;
    showAlert(false);
    setText(titleText);
    showAlert(true);

    dispatch(cartActions.removeItem(id));
  };

  const deleteItem = () => {
    dispatch(cartActions.deleteItem(id));
    deleteIndex(index);
  };

  const onHandleClose = () => {
    showAlert(false);
  }

  return (
    <ListGroupItem className="border-0 cart__item">
      <div className="cart__item-info d-flex gap-2">
        <img src={image01} alt="" />

        <div className="cart__product-info w-100 d-flex align-items-center gap-4 justify-content-between">
          <div>
            <h3 className="cart__product-title">{title}</h3>
            <p className="d-flex align-items-center gap-5 cart__product-price">
              <span>{quantity}x</span> <span>${totalPrice}</span>
            </p>
            <div className=" d-flex align-items-center justify-content-between increase__decrease-btn">
              <button aria-label={`Increase ${title} quantity by 1`} className="ax-button increase__btn" onClick={incrementItem}>
                <i class="ri-add-line"></i>
              </button>
              <span className="quantity">{quantity}</span>
              <button aria-label={`Decrease ${title} quantity by 1`} className="ax-button decrease__btn" onClick={decreaseItem}>
                <i class="ri-subtract-line"></i>
              </button>
            </div>
          </div>

          <button aria-label={`Remove ${title} from cart`} className="ax-button delete__btn" onClick={deleteItem}>
            <i class="ri-close-line"></i>
          </button>
        </div>
      </div>
      {isShowAlert && <CustomizedSnackbars text={text} onCloseAlert={onHandleClose} />}
    </ListGroupItem>
  );
};

export default CartItem;
