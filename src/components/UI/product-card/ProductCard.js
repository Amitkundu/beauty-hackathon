import React, {useState} from "react";
import "../../../styles/product-card.css";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { cartActions } from "../../../store/shopping-cart/cartSlice";
import CustomizedSnackbars from "../../CustomizedSnackbars";

const ProductCard = (props) => {
  const { id, title, files = [], price } = props.item;
  const [disabledState, setdisabledState] = useState(false);
  const dispatch = useDispatch();
  const [isShowAlert, showAlert] = useState(false);

  let image01 = "";

  if (files.length > 0) {
    image01 = files[0]
  }

  const addToCart = () => {
    showAlert(true);
    setdisabledState(true);

    console.log("product Details", image01);
    dispatch(
      cartActions.addItem({
        id,
        title,
        image01,
        price,
      })
    );
  };

  const onHandleClose = () => {
    showAlert(false);
  }

  return (
    <div className="product__item">
      <div className="product__content">
        <div className="product_image_name">
          <div className="product__img">
            <img
              src={image01}
              alt=""
              className="w-50"
            />
            <h3>
              <Link to={`/products/${id}`}>{title}</Link>
            </h3>
          </div>
        </div>
        <div className=" d-flex align-items-center justify-content-between ">
          <span className="product__price">{`$${price}`}</span>
          <button
            className="addTOCart__btn"
            onClick={addToCart}
            type="button"
            aria-label={`Add to Cart - ${title}`}
            disabled={disabledState ? true : false}>
            Add to Cart
          </button>
        </div>
      </div>
      {isShowAlert && <CustomizedSnackbars text={`${title} added to Cart`} onCloseAlert={onHandleClose} />}
    </div>
  );
};

export default ProductCard;
