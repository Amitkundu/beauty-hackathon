import React from "react";
import Slider from "react-slick";
import ava01 from "../../../assets/images/ava-1.jpg";
import ava02 from "../../../assets/images/ava-2.jpg";
import ava03 from "../../../assets/images/ava-3.jpg";
import "../../../styles/slider.css";
const TestimonialSlider = () => {
  const settings = {
    dots: false,
    autoplay: false,
    infinite: true,
    speed: 1000,
    autoplaySpeed: 3000,
    swipeToSlide: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    accessibility: true
  };
  return (
    <Slider {...settings} accessibility={true}>
      <div>
        <p className="review__text">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis
          atque, quam minus totam maiores laborum! Impedit consectetur illum
          aliquid odit. Odit dolore ipsum quod debitis nostrum necessitatibus
          quis dolorem quas!
        </p>
        <div className=" slider__content d-flex align-items-center gap-3 ">
          <img src={ava01} alt="" className=" rounded" />
          <span className="fw-bold">Jhon Doe</span>
        </div>
      </div>
      <div>
        <p className="review__text">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis
          atque, quam minus totam maiores laborum! Impedit consectetur illum
          aliquid odit. Odit dolore ipsum quod debitis nostrum necessitatibus
          quis dolorem quas!
        </p>
        <div className="slider__content d-flex align-items-center gap-3 ">
          <img src={ava02} alt="" className=" rounded" />
          <span className="fw-bold">Mitchell Marsh</span>
        </div>
      </div>
      <div>
        <p className="review__text">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis
          atque, quam minus totam maiores laborum! Impedit consectetur illum
          aliquid odit. Odit dolore ipsum quod debitis nostrum necessitatibus
          quis dolorem quas!
        </p>
        <div className="slider__content d-flex align-items-center gap-3 ">
          <img src={ava03} alt="" className=" rounded" />
          <span className="fw-bold">Steven Crock</span>
        </div>
      </div>
    </Slider>
  );
};

export default TestimonialSlider;
