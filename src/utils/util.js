
const handleFocus = (index) => {
	const cart_list = document.querySelector(".list-group");
	if (cart_list) {
		const allItems = cart_list.querySelectorAll("li");
		if(index === 0 && allItems.length === 0) {
			const noItem = cart_list.querySelector("#no_item");
			if(noItem) noItem.focus();
		}
		else if(index === allItems.length) {
			allItems[index - 1].querySelector(".delete__btn").focus();
		} else if(index >=  (allItems.length - 1)) {
			allItems[index].querySelector(".delete__btn").focus();
		} else {
			const item = allItems[index];
			if (item) item.querySelector(".delete__btn").focus();
		}
	}
}


const addTestimonialProps = () => {
    const prev = document.querySelector(".slick-prev");
    const next = document.querySelector(".slick-next");
    if(prev) {
       
        prev.setAttribute("aria-label", "Previous Testimonial");
    }
    if(next) {
       
        next.setAttribute("aria-label", "Next Testimonial");
    }
}

const reStoreTabIndex = () => {
    var overlay = document.querySelector("#dialog");
    var content = document.querySelector("main");
    var header = document.querySelector("header");
    var footer = document.querySelector("footer");
    var focusable = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]';


    header.removeAttribute("aria-hidden");
    content.removeAttribute("aria-hidden");
    footer.removeAttribute("aria-hidden");

    header.querySelectorAll(focusable).forEach((item) => {
        if(item.hasAttribute("data-tabindex")) {
            item.setAttribute("tabindex", item.getAttribute("data-tabindex"));
            item.removeAttribute('data-tabindex');
        } else {
            item.removeAttribute('tabindex');
        }
    });

    content.querySelectorAll(focusable).forEach((item) => {
        if(item.hasAttribute("data-tabindex")) {
            item.setAttribute("tabindex", item.getAttribute("data-tabindex"));
            item.removeAttribute('data-tabindex');
        } else {
            item.removeAttribute('tabindex');
        }
    });

    footer.querySelectorAll(focusable).forEach((item) => {
        if(item.hasAttribute("data-tabindex")) {
            item.setAttribute("tabindex", item.getAttribute("data-tabindex"));
            item.removeAttribute('data-tabindex');
        } else {
            item.removeAttribute('tabindex');
        }
    });
}

const handleFocusBetweenRemoveBtn = (index) => {
	console.log(index);
	const cart_list = document.querySelector("table");

	if(index === 0 && !cart_list) {
		setTimeout(() => {
			const noItem = document.querySelector("#no_item");
			if(noItem) {
				noItem.focus();
			}
		}, 350);
	}

	if (cart_list) {
		const allItems = cart_list.querySelectorAll(".cart_row");
		if(index === allItems.length - 1) {
			const item = allItems[index];
			if (item) item.querySelector("button").focus();
		} else if(index >=  (allItems.length - 1)) {
			const item = allItems[allItems.length - 1];
			if (item) item.querySelector("button").focus();
		} else {
			const item = allItems[index];
			if (item) item.querySelector("button").focus();
		}
	}
}

const isPhoneNumberValid = (no) => {
	const regex = /^[6-9][0-9]{9}$/;
    return regex.test(no);
}

const isPostCodeValid = (no) => {
	const regex = /^[1-9][0-9]{5}$/;
    return regex.test(no);
}

const handleDialog = () => {

var overlay = document.querySelector("#dialog");
var content = document.querySelector("main");
var header = document.querySelector("header");
var footer = document.querySelector("footer");
	
var focusable = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]';

openDialog();



function overlayOn() {
        overlay.classList.add("active");
        header.setAttribute("aria-hidden", "true");
        content.setAttribute("aria-hidden", "true");
        footer.setAttribute("aria-hidden", "true");

        header.querySelectorAll(focusable).forEach((item) => {
        	if(item.hasAttribute("tabindex")) {
        		item.setAttribute("data-tabindex", item.getAttribute("tabindex"));
        	}
        	item.setAttribute('tabindex', '-1');
        });

        content.querySelectorAll(focusable).forEach((item) => {
        	if(item.hasAttribute("tabindex")) {
        		item.setAttribute("data-tabindex", item.getAttribute("tabindex"));
        	}
        	item.setAttribute('tabindex', '-1');
        });

        footer.querySelectorAll(focusable).forEach((item) => {
        	if(item.hasAttribute("tabindex")) {
        		item.setAttribute("data-tabindex", item.getAttribute("tabindex"));
        	}
        	item.setAttribute('tabindex', '-1');
        });
    }

    function overlayOff() {
        overlay.classList.remove("active");
        header.removeAttribute("aria-hidden");
        content.removeAttribute("aria-hidden");
        footer.removeAttribute("aria-hidden");
      	header.querySelectorAll(focusable).forEach((item) => {
      		if(item.hasAttribute("data-tabindex")) {
        		item.setAttribute("tabindex", item.getAttribute("data-tabindex"));
        		item.removeAttribute('data-tabindex');
        	} else {
        		item.removeAttribute('tabindex');
        	}
        });

        content.querySelectorAll(focusable).forEach((item) => {
      		if(item.hasAttribute("data-tabindex")) {
        		item.setAttribute("tabindex", item.getAttribute("data-tabindex"));
        		item.removeAttribute('data-tabindex');
        	} else {
        		item.removeAttribute('tabindex');
        	}
        });

        footer.querySelectorAll(focusable).forEach((item) => {
      		if(item.hasAttribute("data-tabindex")) {
        		item.setAttribute("tabindex", item.getAttribute("data-tabindex"));
        		item.removeAttribute('data-tabindex');
        	} else {
        		item.removeAttribute('tabindex');
        	}
        });
    }

    function escapeDismiss(e) {
       var keyCode = e.keyCode || e.which;
        if (keyCode == 27) {

          e.preventDefault();
          e.stopPropagation();
            closeDialog();
            return false;
        }
    }

    function bindEvents() {
        document.addEventListener('keydown', escapeDismiss, true); //ESC
       // overlay.addEventListener('click', closeDialog, true); //overlay
	}

	function unbindEvents() {
        document.removeEventListener('keydown', escapeDismiss, true); //ESC
        //overlay.removeEventListener('click', closeDialog, true); 
	}

    function openDialog() {
		focusable = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]';
		overlayOn();
		document.addEventListener('keydown', escapeDismiss, true);
	}

    function closeDialog() {
        overlayOff();
        unbindEvents();
        const closeBtn = document.querySelector("#closeDialog");
        
        if(closeBtn) {
        	closeBtn.click();
        	setTimeout(() => {
        		const triggerBtn = document.querySelector("#triggerBtn");
        		if(triggerBtn) triggerBtn.focus();
        	}, 350);
        }
       
    	return false;
	}

}

export { handleFocus, handleFocusBetweenRemoveBtn, isPhoneNumberValid, isPostCodeValid, handleDialog, addTestimonialProps, reStoreTabIndex }