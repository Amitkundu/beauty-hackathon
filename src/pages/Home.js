import React, { useState, useEffect } from "react";
import Helmet from "../components/Helmet/Helmet.js";
import { Container, Row, Col, ListGroup, ListGroupItem } from "reactstrap";
import heroImg from "../assets/images/hero.png";
import "../styles/hero-section.css";
import { Link } from "react-router-dom";
import "../styles/home.css";
import featureImg01 from "../assets/images/service-01.png";
import featureImg02 from "../assets/images/service-02.png";
import featureImg03 from "../assets/images/service-03.png";
import {products, popular_food} from "../assets/fake-data/Sample_products.js";

import ProductCard from "../components/UI/product-card/ProductCard.js";
import whyImg from "../assets/images/location.png";
import networkImg from "../assets/images/network.png";
import TestimonialSlider from "../components/UI/slider/TestimonialSlider.js";
import { display, width } from "@mui/system";
import {addTestimonialProps} from '../utils/util.js';

const featureData = [
  {
    title: "FREE SHIPPING",
    imgUrl: featureImg02,
    desc: "Order in for yourself or for the group, and get free delivery on orders above $10",
  },
  {
    title: "EASY RETURNS",
    imgUrl: featureImg03,
    desc: "15-Day Return Policy.",
  },
  {
    title: "Quick Delivery",
    imgUrl: featureImg01,
    desc: "Experience our superfast delivery of all skin products.",
  },
];

const Home = () => {
  const [category, setCategory] = useState("All");
  const [allProducts, setAllProducts] = useState(products);
  const [todaysOffer, setTodaysOffer] = useState([]);

  useEffect(() => {
    const everyday = products.filter((item) => item.category === "EVERYDAY");
    const slicePizza = everyday.slice(0, 4);
    setTodaysOffer(everyday);
    addTestimonialProps();
  }, []);

  useEffect(() => {
    if (category === "All") {
      setAllProducts(products);
    }

    if (category === "Makeup") {
      const filteredProducts = products.filter(
        (item) => item.category === "Makeup"
      );

      setAllProducts(filteredProducts);
    }

    if (category === "Lip Care") {
      const filteredProducts = products.filter(
        (item) => item.category === "Lip Care"
      );

      setAllProducts(filteredProducts);
    }

    if (category === "Perfumes") {
      const filteredProducts = products.filter(
        (item) => item.category === "Perfumes"
      );

      setAllProducts(filteredProducts);
    }
  }, [category]);

  return (
    <Helmet title="Home">
      <section>
        <Container>
          <Row>
            <Col lg="6" md="6">
              <div className="hero__content  ">
                <span className="mb-3 fw-lighter" id="main" tabIndex="-1">Everything is There For Your Beauty Needs</span>
                <h1 className="mb-4 hero__title">
                  <span>More Than Makeups! <br /></span> We Create Beauty in Joy <span> Give The Best Magic Beauty</span>
                </h1>

                <p>
                  Our job is to take care your skin tones and make yourself beautiful.
                </p>

                <div className="hero__btns d-flex align-items-center gap-5 mt-4">
                  <span className="all__foods-btn">
                    <Link to="/products">See all products</Link>
                  </span>
                </div>

                <div className=" hero__service  d-flex align-items-center gap-5 mt-5 ">
                  <p className=" d-flex align-items-center gap-2 ">
                    <span className="shipping__icon">
                      <i class="ri-car-line"></i>
                    </span>{" "}
                    No shipping charge
                  </p>

                  <p className=" d-flex align-items-center gap-2 ">
                    <span className="shipping__icon">
                      <i class="ri-shield-check-line"></i>
                    </span>{" "}
                    100% secure checkout
                  </p>
                </div>
              </div>
            </Col>

            <Col lg="6" md="6">
              <div className="hero__img">
                <img src={heroImg} alt="" className="w-100" />
              </div>
            </Col>
          </Row>
        </Container>
      </section>

      <section>
        <Container>
          <Row>
            <Col lg="12" className="text-center">
              <h2 className="feature__subtitle mb-4">What we serve</h2>
              <h3 className="feature__title">Just sit back at home
              <div className="feature__title">
                we will <span>take care</span>
              </div>
              </h3>
              <p className="mb-1 mt-4 feature__text">
                When you are too lazy to visit shops, we are just a click away.
              </p>
              <p className="feature__text">
               Discover our best high quality and variety skin care beauty products.
              </p>
            </Col>

            {featureData.map((item, index) => (
              <Col
                lg="4"
                md="6"
                sm="6"
                key={index}
                className="mt-5">
                <div className="feature__item text-center px-5 py-3">
                  <img
                    src={item.imgUrl}
                    alt=""
                    className="w-25 mb-3"
                  />
                  <h4 className=" fw-bold mb-3">{item.title}</h4>
                  <p>{item.desc}</p>
                </div>
              </Col>
            ))}
          </Row>
        </Container>
      </section>

      <section>
        <Container>
          <Row>
            <Col
              lg="12"
              className="text-center">
              <h2>Maximize Your Beauty</h2>
            </Col>

            <Col lg="12">
              <ul
                role="tablist"
                aria-label="Food Category"
                className="food__category d-flex align-items-center justify-content-center gap-4">
                
                  {popular_food.map((item, index) => (
                    <li role="none">
                    <button
                      role="tab"
                      id={item.category}
                      aria-controls={`${category}_tabpanel`}
                      className={`all__btn  ${
                        category === item.category ? "foodBtnActive" : ""
                      } `}
                      aria-selected={
                        category === item.category ? "true" : "false"
                      }
                      onClick={() => setCategory(item.category)}>
                      {item.image !== "" && (
                        <img
                          src={item.image}
                          alt=""
                          className=""
                        />
                      )}
                      <span className="px-1">{item.title}</span>
                    </button>
                    </li>
                  ))}
                
              </ul>
            </Col>
          </Row>
          <Row
            role="tabpanel"
            aria-labelledby={category}
            id={`${category}_tabpanel`}>
            {allProducts.map((item) => (
              <Col
                lg="3"
                md="4"
                sm="6"
                xs="6"
                key={item.id}
                className="mt-5">
                <ProductCard item={item} />
              </Col>
            ))}
          </Row>
        </Container>
      </section>

      <section className="why__choose-us">
        <Container>
          <Row>
            <Col
              lg="6"
              md="6">
              <img
                src={whyImg}
                alt=""
                className="w-100"
              />
            </Col>

            <Col
              lg="6"
              md="6">
              <div className="why__tasty-treat">
                <h2 className="tasty__treat-title mb-4">
                  Why <span>Beautify?</span>
                </h2>
                <p className="tasty__treat-desc">
                  Hungry! Let's browse Testy treat. From Quick setup to Instant Order Notofications, Dynamic Pricing. Instant Payment.
                </p>

                <ListGroup className="mt-4">
                  <ListGroupItem className="border-0 ps-0">
                    <h3 className=" choose__us-title d-flex align-items-center gap-2 ">
                      <i aria-hidden="true" className="ri-checkbox-circle-line"></i>
                      Resonable high quality products
                    </h3>
                    <p className="choose__us-desc">
                      We provide all high quality lost cost products.
                    </p>
                  </ListGroupItem>

                  <ListGroupItem className="border-0 ps-0">
                    <h3 className="choose__us-title d-flex align-items-center gap-2 ">
                      <i aria-hidden="true" className="ri-checkbox-circle-line"></i> No minimim order amount
                    </h3>
                    <p className="choose__us-desc">
                      There is no restrictions on order amount
                    </p>
                  </ListGroupItem>

                  <ListGroupItem className="border-0 ps-0">
                    <h3 className="choose__us-title d-flex align-items-center gap-2 ">
                      <i aria-hidden="true" className="ri-checkbox-circle-line"></i>Order from any
                      location
                    </h3>
                    <p className="choose__us-desc">
                     You can order any product from any location at any time.
                    </p>
                  </ListGroupItem>
                </ListGroup>
              </div>
            </Col>
          </Row>
        </Container>
      </section>

      <section className="pt-0">
        <Container>
          <Row>
            <Col
              lg="12"
              className="text-center mb-5 ">
              <h2>Today's Offer</h2>
            </Col>

            {todaysOffer.map((item) => (
              <Col
                lg="3"
                md="4"
                sm="6"
                xs="6"
                key={item.id}>
                <ProductCard item={item} />
              </Col>
            ))}
          </Row>
        </Container>
      </section>

      <section>
        <Container>
          <Row>
            <Col
              lg="6"
              md="6">
              <div className="testimonial px-4">
                <h2 className="testimonial__subtitle mb-4">Testimonial</h2>
                <h3 className="testimonial__title mb-4">
                  What our <span>customers</span> are saying
                </h3>
                <p className="testimonial__desc">
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                  Distinctio quasi qui minus quos sit perspiciatis inventore
                  quis provident placeat fugiat!
                </p>

                <TestimonialSlider />
              </div>
            </Col>

            <Col
              lg="6"
              md="6">
              <img
                src={networkImg}
                alt=""
                className="w-100"
              />
            </Col>
          </Row>
        </Container>
      </section>

    </Helmet>
  );
};

export default Home;
