import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import { Container, Row, Col } from "reactstrap";
import CommonSection from "../components/UI/common-section/CommonSection";
import Helmet from "../components/Helmet/Helmet";
import {isPhoneNumberValid, isPostCodeValid} from '../utils/util.js';
import "../styles/checkout.css";
const Checkout = () => {

  const initialAddressDetails = {
    name : "",
    email : "",
    phonenumber : "",
    country: "",
    city: "",
    postalcode: "",
  };

  const [addressDetails, setAddessDetails] = useState(initialAddressDetails);

  const initialAddressError = {
    nameError : false,
    emailError : false,
    phonenumberError : false,
    countryError: false,
    cityError: false,
    postalcodeError: false,
  };

  const [addressError, setAddessError] = useState(initialAddressError);
  const [isInvalidForm, setInvalidForm] = useState(false);
  const [isPayment, setPayment] = useState(false);
  const headingRef = useRef(null);

  const shippingInfo = [];
  const cartTotalAmount = useSelector((state) => state.cart.totalAmount);
  const shippingCost = 30;
  const totalAmount = cartTotalAmount + Number(shippingCost);

  useEffect(() => {
    if(isInvalidForm) {
      const ele = document.querySelector('[aria-invalid = "true"]');
      if (ele) {        
        ele.focus();
        setInvalidForm(false);
      }
    }
  }, [isInvalidForm]);

  const submitHandler = (e) => {
    e.preventDefault();
    const isFormValid = validate();
    if(isFormValid) {
      setInvalidForm(false);
      setPayment(true);

    } else {
      setInvalidForm(true);
    }
   
  };

  useEffect(() => {
    if(isPayment) {
      if(headingRef && headingRef.current) {
        headingRef.current.focus();
      }
    }
  }, [isPayment]);

  const onInputChange = (e) => {
    setAddessDetails({...addressDetails, [e.target.name] : e.target.value});
  }
 
  const validate = () => {
    let isFormValid = true;
    let inValidName = false;
    let inValidEmail = false;
    let inValidPhoneNumber = false;
    let inValidCountry = false;
    let inValidCity = false;
    let inValidPostalCode = false;

    if(name === "") {
      inValidName = true;
      isFormValid = false;
    }
    if(email === "") {
      inValidEmail = true;
      isFormValid = false;
    }

    if(phonenumber === "") {
      inValidPhoneNumber = true;
      isFormValid = false;
    } else if(!isPhoneNumberValid(phonenumber)) {
      inValidPhoneNumber = true;
      isFormValid = false;
    }

    if(country === "") {
      inValidCountry = true;
      isFormValid = false;
    }

    if(city === "") {
      inValidCity = true;
      isFormValid = false;
    }

    if(postalcode === "") {
      inValidPostalCode = true;
      isFormValid = false;
    } else if(!isPostCodeValid(postalcode)) {
      inValidPostalCode = true;
      isFormValid = false;
    }

    setAddessError({...addressError, 
      nameError : inValidName,
      emailError : inValidEmail,
      phonenumberError : inValidPhoneNumber,
      countryError: inValidCountry,
      cityError: inValidCity,
      postalcodeError: inValidPostalCode
    })

    return isFormValid;
  }

  const {
    name = "",
    email = "",
    phonenumber = "",
    country = "",
    city = "",
    postalcode = "",
  } = addressDetails;

  const {
    nameError = false,
    emailError = false,
    phonenumberError = false,
    countryError = false,
    cityError = false,
    postalcodeError = false
  } = addressError;

  return (
    <Helmet title="Checkout">
      <CommonSection title="Checkout" />
      <section>
        <Container>
          <Row>
            <Col lg="8" md="6">
              {!isPayment && <>
              <h2 className="mb-4 shipping_address">Shipping Address</h2>
              <form className="checkout__form" onSubmit={submitHandler}>
                <div className="form__group">
                  <label for="name">Enter your name</label>
                  <input
                    id="name"
                    name="name"
                    type="text"
                    placeholder="eg. John deo"
                    aria-required="true"
                    value={name}
                    aria-invalid={nameError}
                    onChange={(e) => onInputChange(e)}
                    aria-describedby={nameError ? "name_error" : ""}
                  />
                  {nameError && <div id="name_error" className="text-danger">! Provide a valid user Name</div>}
                </div>

                <div className="form__group">
                  <label for="email">Enter your email</label>
                  <input
                    id="email"
                    type="email"
                    name="email"
                    placeholder="eg. John@xxx.com"
                    aria-required="true"
                    value={email}
                    aria-invalid={emailError}
                    onChange={(e) => onInputChange(e)}
                    aria-describedby={emailError ? "email_error" : ""}
                  />
                  {emailError && <div id="email_error" className="text-danger">! Provide a valid Email Id</div>}
                </div>
                <div className="form__group">
                  <label for="number">Enter your phone number</label>
                  <input
                    id="number"
                    type="number"
                    name="phonenumber"
                    placeholder="10-digit mobile number"
                    aria-required="true"
                    aria-invalid={phonenumberError}
                    onChange={(e) => onInputChange(e)}
                    aria-describedby={phonenumberError ? "ph_error" : ""}
                  />
                  {phonenumberError && <div id="ph_error" className="text-danger">! Provie a valid Phone Number</div>}
                </div>
                <div className="form__group">
                  <label for="country">Enter your country</label>
                  <input
                    id="country"
                    type="text"
                    placeholder="eg. India"
                    name="country"
                    aria-required="true"
                    aria-invalid={countryError}
                    onChange={(e) => onInputChange(e)}
                    aria-describedby={countryError ? "country_error" : ""}
                  />
                  {countryError && <div id="country_error" className="text-danger">! Provide a valid Country name</div>}
                </div>
                <div className="form__group">
                  <label for="city">Enter your city</label>
                  <input
                    id="city"
                    type="text"
                    name="city"
                    placeholder="Delhi"
                    aria-required="true"
                    aria-invalid={cityError}
                    onChange={(e) => onInputChange(e)}
                    aria-describedby={cityError ? "city_error" : ""}
                  />
                  {cityError && <div id="city_error" className="text-danger">! Provide a valid City name</div>}
                </div>
                <div className="form__group">
                  <label for="postcode">Enter your postal code</label>
                  <input
                    id="postcode"
                    type="number"
                    name="postalcode"
                    placeholder="Postal code"
                    aria-required="true"
                    aria-invalid={postalcodeError}
                    onChange={(e) => onInputChange(e)}
                    aria-describedby={postalcodeError ? "postal_error" : ""}
                  />
                  {postalcodeError && <div id="postal_error" className="text-danger">! Provide a valid Postal code</div>}
                </div>
                <button type="submit" className="addTOCart__btn">
                  Payment
                </button>
              </form>
              </>}
              {isPayment && 
                <>
                  <h2 tabIndex="-1" id="payment" ref={headingRef}>Thank you for your Order</h2>
                  <p className="shipping_address">Pay <span className="feature__subtitle fw-bold">${totalAmount}</span> to our delivery friend !!!</p>
                  <p>We would like to see you soon.</p>
               </>
              }
            </Col>

            {!isPayment && <Col lg="4" md="6">
              <div className="checkout__bill">
                <h2 className="d-flex align-items-center justify-content-between mb-3">
                  Subtotal: <span>${cartTotalAmount}</span>
                </h2>
                <h2 className="d-flex align-items-center justify-content-between mb-3">
                  Shipping: <span>${shippingCost}</span>
                </h2>
                <div className="checkout__total">
                  <h2 className="d-flex align-items-center justify-content-between">
                    Total: <span>${totalAmount}</span>
                  </h2>
                </div>
              </div>
            </Col>}
          </Row>
        </Container>
      </section>
    </Helmet>
  );
};

export default Checkout;
