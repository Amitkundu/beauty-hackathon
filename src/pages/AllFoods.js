import React, { useState, useEffect, useRef } from "react";
import Helmet from "../components/Helmet/Helmet";
import CommonSection from "../components/UI/common-section/CommonSection";
import { Container, Row, Col } from "reactstrap";
import {products} from "../assets/fake-data/Sample_products";
import ProductCard from "../components/UI/product-card/ProductCard";
import ReactPaginate from "react-paginate";
import "../styles/all-foods.css";
import "../styles/pagination.css";

const AllFoods = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [pageNumber, setPageNumber] = useState(0);
  const [sortBy, setSortBy] = useState("ascending");
  const liveRegionRef = useRef(null);

  const [searchedProduct, setSearchProduct] = useState(products);
  let searchStatus = document.getElementById("search-status");
  let paginationStatus = document.getElementById("pagination-status");


  const performFilter = () => {
    let filterProduct = products;
    if (searchTerm.value !== "") {
      filterProduct = products.filter((item) => {
      
        if (item.title.toLowerCase().includes(searchTerm.toLowerCase())) {
          return item;
        } else {
          return;
        }
      });
    }

    if(sortBy === 'ascending') {
      filterProduct = filterProduct.sort(function(a,b) {
        if(a.title > b.title) {
          return 1
        } else if (a.price < b.price) {
          return -1
        }
        return 0;
      });
    }
    else if(sortBy === 'descending') {
      filterProduct = filterProduct.sort(function(a,b) {
        if(a.title > b.title) {
          return -1
        } else if (a.price < b.price) {
          return 1
        }
        return 0;
      });
    }
    else if(sortBy === "high") {

      filterProduct = filterProduct.sort(function(a,b) {
        if(a.price > b.price) {
          return -1
        } else if (a.price < b.price) {
          return 1
        }
        return 0;
      });

    } else if(sortBy === "low") {
      filterProduct = filterProduct.sort(function(a,b) {
        if(a.price > b.price) {
          return 1
        } else if (a.price < b.price) {
          return -1
        }
        return 0;
      });
    } else {
      console.log("nothing to update")
    }



    setSearchProduct(filterProduct);
  }

  /*const searchedProduct = products.filter((item) => {
    if (searchTerm.value === "") {
      return item;
    }
    if (item.title.toLowerCase().includes(searchTerm.toLowerCase())) {
      return item;
    } else {
      return console.log("not found");
    }
  });*/

  useEffect(() => {
    performFilter();
    updateLiveRegion();
  }, [searchTerm, sortBy]);

  const productPerPage = 20;
  const visitedPage = pageNumber * productPerPage;
  const displayPage = searchedProduct.slice(
    visitedPage,
    visitedPage + productPerPage
  );

  const pageCount = Math.ceil(searchedProduct.length / productPerPage);
  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const updateLiveRegion = () => {
    const numberOfProduct = searchedProduct.length;

    if(liveRegionRef && liveRegionRef.current) {

      if(numberOfProduct) {
        liveRegionRef.current.innerText = numberOfProduct > 1 ? `Showing page ${pageNumber + 1}, ${(pageNumber * productPerPage + 1)} to ${(pageNumber * productPerPage) + displayPage.length} products out of ${searchedProduct.length}` : `Showing page ${pageCount}, ${displayPage} products of ${searchedProduct.length}`;
      } else {
        liveRegionRef.current.innerText = "No result found"
      }
      
      window.setTimeout(() => {
        if(liveRegionRef && liveRegionRef.current) {
          liveRegionRef.current.innerText = "";
        }
      }, 5000);
    }
  }

  useEffect(() => {
   updateLiveRegion();
  }, [pageNumber]);

  useEffect(() => {
    
  }, []);

  return (
    <Helmet title="All Products">
      <CommonSection title="All Products" />

      <section>
        <Container>
          <Row>
            <Col lg="6" md="6" sm="6" xs="12">
              <div className="search__widget d-flex align-items-center justify-content-between ">
                <input
                  type="text"
                  placeholder="I'm looking for...."
                  value={searchTerm}
                  aria-label="Search a product"
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
                <span>
                  <i class="ri-search-line"></i>
                </span>
              </div>
            </Col>
            <Col lg="6" md="6" sm="6" xs="12" className="mb-5">
              <div className="sorting__widget text-end">
                <label for="sortby" className="a11y">Sort by</label>
                <select id="sortby" lassName="w-50" onChange={(e) => setSortBy(e.target.value)}>
                  <option disabled>Default</option>
                  <option value="ascending">Alphabetically, A-Z</option>
                  <option value="descending">Alphabetically, Z-A</option>
                  <option value="high">High Price</option>
                  <option value="low">Low Price</option>
                </select>
              </div>
            </Col>
            <h2 className="a11y">Products</h2>
            {displayPage.map((item) => (
              <Col lg="3" md="4" sm="6" xs="6" key={item.id} className="mb-4">
                <ProductCard item={item} />
              </Col>
            ))}

            <nav className="pagination" aria-label="Pagination">
              <ReactPaginate
                pageCount={pageCount}
                onPageChange={changePage}
                previousLabel={"Prev"}
                nextLabel={"Next"}
                containerClassName=" paginationBttns "
              />
            </nav>
          </Row>
        </Container>
      </section>
     <div
        className="a11y"
        ref={liveRegionRef}
        role="status"
        id="search-status"></div>
    </Helmet>
  );
};

export default AllFoods;
