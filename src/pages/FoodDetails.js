import React, { useState, useEffect } from "react";
import {products} from "../assets/fake-data/Sample_products";
import { useParams } from "react-router-dom";
import Helmet from "../components/Helmet/Helmet";
import CommonSection from "../components/UI/common-section/CommonSection";
import { Container, Row, Col } from "reactstrap";
import { useDispatch } from "react-redux";
import { cartActions } from "../store/shopping-cart/cartSlice";
import "../styles/product-details.css";
import ProductCard from "../components/UI/product-card/ProductCard";
import CustomizedSnackbars from "../components/CustomizedSnackbars";

const FoodDetails = () => {
  const [tab, setTab] = useState("desc");
  const [enteredName, setEnteredName] = useState("");
  const [enteredEmail, setEnteredEmail] = useState("");
  const [reviewMsg, setReviewMsg] = useState("");
  const { id } = useParams();

  const dispatch = useDispatch();
  const product = products.find((product) => product.id == id);

  const [previewImg, setPreviewImg] = useState("");
  const { title ="", price = 0, category = "", desc = "", files = [] } = product;
  const [disabledState, setdisabledState] = useState(false);
  const [isShowAlert, showAlert] = useState(false);

  let imageFile = "";

  useEffect(() => {
    if (files.length > 0) {
      imageFile = files[0]
      setPreviewImg(imageFile)
    }
    
  }, []);

  const relatedProduct = products.filter((item) => category === item.category);

  const initialReviewDetails = {
    name : "",
    email : "",
    review : ""
  };

  const [reviewDetails, setReviewDetails] = useState(initialReviewDetails);

  const initialReviewError = {
    nameError : false,
    emailError : false,
    reviewError: false
  };

  const [reviewsError, setReviewError] = useState(initialReviewError);
  const [isInvalidForm, setInvalidForm] = useState(false);

  useEffect(() => {
    if(tab === "rev") {
      if(isInvalidForm) {
        const ele = document.querySelector('[aria-invalid = "true"]');
        if (ele) {          
          ele.focus();
          setInvalidForm(false);
        }
      }
    }
  }, [isInvalidForm]);

  const submitHandler = (e) => {
    e.preventDefault();
    const isFormValid = validate();
    if(isFormValid) {
      setInvalidForm(false)
    } else {
      setInvalidForm(true)
    }
  };

  const onInputChange = (e) => {
    setReviewDetails({...reviewDetails, [e.target.name] : e.target.value});
  }
 
  const validate = () => {
    let isFormValid = true;
    let inValidName = false;
    let inValidEmail = false;
    let inValidReview = false;

    if(name === "") {
      inValidName = true;
      isFormValid = false;
    }
    if(email === "") {
      inValidEmail = true;
      isFormValid = false;
    }

    if(review === "") {
      inValidReview = true;
      isFormValid = false;
    }

    setReviewError({...reviewsError, 
      nameError : inValidName,
      emailError : inValidEmail,
      reviewError : inValidReview
    })

    return isFormValid;
  }

  const {
    name = "",
    email = "",
    review = "",
  } = reviewDetails;

  const {
    nameError = false,
    emailError = false,
    reviewError = false,
  } = reviewsError;


  const addItemToCart = () => {
    showAlert(true);
    setdisabledState(true);

    dispatch(
      cartActions.addItem({
        id,
        title,
        price,
        imageFile,
      })
    );
  };

  useEffect(() => {
    setPreviewImg(imageFile);
  }, [product]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [product]);

  const onHandleClose = () => {
    showAlert(false);
  }

  console.log(files);

  return (
    <Helmet title="Product details">
      <CommonSection title={title} />
      <section>
        <Container>
          <Row>
            <Col lg="2" md="2">

              <div className="product__images" role="group" aria-label="Product view">
                {files.length !== 0 && files.map((item) => (
                  <button
                    aria-current = {previewImg === item ? "true" : "false"}
                    type="button"
                    className="ax-button img__item mb-3"
                    onClick={() => setPreviewImg(item)}
                  >
                    <img src={item} alt="Product 1" className="w-50" />
                  </button>
                ))}
              </div>
            </Col>

            <Col lg="4" md="4">
              <div className="product__main-img">
                <img src={previewImg} alt="Product View" className="w-100" />
              </div>
            </Col>

            <Col lg="6" md="6">
              <div className="single__product-content">
                <h2 className="product__title mb-3">{title}</h2>
                <p className="product__price">
                  {" "}
                  Price: <span>${price}</span>
                </p>
                <p className="category mb-5">
                  Category: <span>{category}</span>
                </p>

                <button type="button" disabled={disabledState} onClick={addItemToCart} className="addTOCart__btn">
                  Add to Cart
                </button>
              </div>
            </Col>

            <Col lg="12">
              <div role="tablist" aria-label="Product details" className="tabs d-flex align-items-center gap-5 py-3">
                <button
                  type="button"
                  id="desc"
                  role="tab"
                  aria-controls="desc_panel"
                  aria-selected={`${tab === "desc" ? "true" : "false"}`}
                  className={`ax-button ${tab === "desc" ? "tab__active" : ""}`}
                  onClick={() => setTab("desc")}
                >
                  Description
                </button>
                <button
                  type="button"
                  id="revw"
                  role="tab"
                  aria-controls="revw_panel"
                  aria-selected={`${tab === "rev" ? "true" : "false"}`}
                  className={`ax-button ${tab === "rev" ? "tab__active" : ""}`}
                  onClick={() => setTab("rev")}
                >
                  Review
                </button>
              </div>

              {tab === "desc" ? (
                <div role="tabpanel" aria-labelledby="desc" id="desc_panel" className="tab__content">
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta ad et est, fugiat repudiandae neque illo delectus commodi magnam explicabo autem voluptates eaque velit vero facere mollitia. Placeat rem, molestiae error obcaecati enim doloribus impedit aliquam, maiores qui minus neque.
  </p>
                </div>
              ) : (
                <div role="tabpanel" aria-labelledby="revw" id="revw_panel" className="tab__form mb-3">
                  <div className="review pt-5">
                    <p className="user__name mb-0">Jhon Doe</p>
                    <p className="user__email">jhon1@gmail.com</p>
                    <p className="feedback__text">great product</p>
                  </div>

                  <div className="review">
                    <p className="user__name mb-0">Jhon Doe</p>
                    <p className="user__email">jhon1@gmail.com</p>
                    <p className="feedback__text">great product</p>
                  </div>

                  <div className="review">
                    <p className="user__name mb-0">Jhon Doe</p>
                    <p className="user__email">jhon1@gmail.com</p>
                    <p className="feedback__text">great product</p>
                  </div>

                  <form className="form" onSubmit={(e) => submitHandler(e)}>
                    <div className="form__group">
                      <label for="name">Enter your name</label>
                      <input
                        id="name"
                        type="text"
                        aria-required="true"
                        placeholder="eg.John deo"
                        name="name"
                        value={name}
                        onChange={(e) => onInputChange(e)}
                        aria-invalid={nameError}
                        aria-describedby={nameError ? "name_error" : ""}
                      />
                      {nameError && <div id="name_error" className="text-danger">! Name is a required field</div>}
                    </div>

                    <div className="form__group">
                      <label for="email">Enter your email</label>
                      <input
                        id="email"
                        type="text"
                        placeholder="eg. john@xxx.com"
                        aria-required="true"
                        name="email"
                        value={email}
                        onChange={(e) => onInputChange(e)}
                        aria-invalid={emailError}
                        aria-describedby={emailError ? "email_error" : ""}
                      />
                      {emailError && <div id="email_error" className="text-danger">! Email is a required field</div>}
                    </div>

                    <div className="form__group">
                      <label for="comments">Write your review</label>
                      <textarea
                        id="comments"
                        rows={5}
                        type="text"
                        placeholder="comments"
                        
                        aria-required="true"
                        name="email"
                        value={email}
                        onChange={(e) => onInputChange(e)}
                        aria-invalid={reviewError}
                        aria-describedby={reviewError ? "review_error" : ""}
                      />
                      {reviewError && <div id="review_error" className="text-danger">! Review is a required field</div>}
                    </div>

                    <button type="submit" className="addTOCart__btn">
                      Submit
                    </button>
                  </form>
                </div>
              )}
            </Col>

            <Col lg="12" className="mb-5 mt-4">
              <h2 className="related__Product-title">You might also like</h2>
            </Col>

            {relatedProduct.map((item) => (
              <Col lg="3" md="4" sm="6" xs="6" className="mb-4" key={item.id}>
                <ProductCard item={item} />
              </Col>
            ))}
          </Row>
        </Container>
      </section>
      {isShowAlert && <CustomizedSnackbars text={`${title} added to Cart`} onCloseAlert={onHandleClose} />}
    </Helmet>
  );
};

export default FoodDetails;
