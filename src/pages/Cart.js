import React, {useState, useEffect} from "react";
import CommonSection from "../components/UI/common-section/CommonSection";
import Helmet from "../components/Helmet/Helmet";
import "../styles/cart-page.css";
import { useSelector, useDispatch } from "react-redux";
import { Container, Row, Col } from "reactstrap";
import { cartActions } from "../store/shopping-cart/cartSlice";
import { Link } from "react-router-dom";
import { handleFocusBetweenRemoveBtn } from "../utils/util.js";

const Cart = () => {
  const cartItems = useSelector((state) => state.cart.cartItems);
  const totalAmount = useSelector((state) => state.cart.totalAmount);
  const [removeIndex, setRemoveIndex] = useState(-1);

  const handleDeleteIndex = (val) => {
    setRemoveIndex(val);
  }

  useEffect(() => {
    if (removeIndex !== -1) {
      handleFocusBetweenRemoveBtn(removeIndex);
      setRemoveIndex(-1);
    }
  }, [cartItems]);

  console.log(cartItems);

  return (
    <Helmet title="Cart">
      <CommonSection title="Your Cart" />
      <section>
        <Container>
          <Row>
            <Col lg="12">
              {cartItems.length === 0 ? (
                <h2 className="text-center" id="no_item" tabIndex="-1">Your cart is empty</h2>
              ) : (
                <table className="table table-bordered" aria-label="Cart details">
                  <thead>
                    <tr>
                      <th>Image</th>
                      <th>Product Title</th>
                      <th>Price</th>
                      <th>Quantity</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {cartItems.map((item, index) => (
                      <Tr index={index} item={item} key={item.id} onDeleteIndex={(indx) => handleDeleteIndex(indx)} />
                    ))}
                  </tbody>
                </table>
              )}

              <div className="mt-4">
                <h2>
                  Subtotal:
                  <span className="cart__subtotal">$ {totalAmount}</span>
                </h2>
                <p>Taxes and shipping will calculate at checkout</p>
                <div className="cart__page-btn">
                  <span className="addTOCart__btn me-4">
                    <Link to="/products">Continue Shopping</Link>
                  </span>
                  {totalAmount !== 0 && <span className="addTOCart__btn">
                    <Link to="/checkout">Proceed to checkout</Link>
                  </span>}
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </Helmet>
  );
};

const Tr = (props) => {
  const {index, onDeleteIndex} = props;

  const { id, image01, title, price, quantity } = props.item;
  const dispatch = useDispatch();

  const deleteItem = (inx) => {
    onDeleteIndex(inx);
    dispatch(cartActions.deleteItem(id));
  };

  return (
    <tr className="cart_row">
      <td className="text-center cart__img-box">
        <img src={image01} alt="" />
      </td>
      <td className="text-center">{title}</td>
      <td className="text-center">${price}</td>
      <td className="text-center">{quantity}</td>
      <td className="text-center cart__item-del">
        <button class="ax-button ri-delete-bin-line" aria-label={`Delete ${title}`} onClick={() => deleteItem(index)}></button>
      </td>
    </tr>
  );
};

export default Cart;
