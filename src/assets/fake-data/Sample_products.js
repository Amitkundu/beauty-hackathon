const products = [
  {
    "id": 1,
    "title": "BEST SELLING BESTIES KIT",
    "category": "KIT",
    "price": 35,
    "mrp": 3200,
    "files": ["/images/KIT/KIT_1_1.jpeg", "/images/KIT/KIT_1_2.jpeg"]
  },
  {
    "id": 2,
    "title": "SCRUB UP KIT",
    "category": "KIT",
    "price": 15,
    "mrp": 1500,
    "files": ["/images/KIT/KIT_2_1.jpeg", "/images/KIT/KIT_2_2.jpeg"]
  },
  {
    "id": 3,
    "title": "THE ORIGINAL LIP DUO",
    "category": "Lip Care",
    "price": 12,
    "mrp": 1200,
    "files": ["/images/SKIN_CARE/SKIN_1_1.jpeg", "/images/SKIN_CARE/SKIN_1_2.jpeg"]
  },
  {
    "id": 4,
    "title": "THE MEGA COMPLEXION CREW KIT",
    "category": "KIT",
    "price": 31,
    "mrp": 2900,
    "files": ["/images/KIT/KIT_3_1.jpeg", "/images/KIT/KIT_3_2.jpeg"]
  },
  {
    "id": 5,
    "title": "THE SHOWER SCRUBS KIT",
    "category": "KIT",
    "price": 31,
    "mrp": 12000,
    "files": ["/images/KIT/KIT_4_1.jpeg", "/images/KIT/KIT_4_2.jpeg"]
  },
  {
    "id": 6,
    "title": "GET ACTIVE FACE KIT",
    "category": "Skin Care",
    "price": 30,
    "mrp": 3800,
    "files": ["/images/SKIN_CARE/SKIN_2_1.jpeg", "/images/SKIN_CARE/SKIN_2_2.jpeg"]
  },
  {
    "id": 7,
    "title": "THE ORIGINAL SCRUB CLUB",
    "category": "KIT",
    "price": 48,
    "mrp": 4500,
    "files": ["/images/KIT/KIT_5_1.jpeg", "/images/KIT/KIT_5_2.jpeg"]
  },
  {
    "id": 8,
    "title": "BEST FACE FORWARD KIT",
    "category": "Skin Care",
    "price": 60,
    "mrp": 8000,
    "files": ["/images/SKIN_CARE/SKIN_3_1.jpeg", "/images/SKIN_CARE/SKIN_3_2.jpeg"]
  },
  {
    "id": 9,
    "title": "NON-STOP HAIR DUO KIT",
    "category": "HAIR",
    "price": 17,
    "mrp": 2399,
    "files": ["/images/HAIR/HAIR_1_1.jpeg", "/images/HAIR/HAIR_1_2.jpeg"]
  },
  {
    "id": 10,
    "title": "THE BABY MUMMA KIT",
    "category": "KIT",
    "price": 30,
    "mrp": 2876,
    "files": ["/images/KIT/KIT_6.jpeg"]
  },
  {
    "id": 11,
    "title": "THE OUT OF TOWNERS KIT",
    "category": "KIT",
    "price": 320400,
    "mrp": 3500,
    "files": ["/images/KIT/KIT_7_1.jpeg", "/images/KIT/KIT_7_2.jpeg"]
  },
  {
    "id": 12,
    "title": "THE PORE-IFICS KIT",
    "category": "KIT",
    "price": 32,
    "mrp": 3200,
    "files": ["/images/KIT/KIT_8_1.jpeg", "/images/KIT/KIT_8_2.jpeg"]
  },
  {
    "id": 13,
    "title": "BUTT FIRST, COFFEE KIT",
    "category": "KIT",
    "price": 18,
    "mrp": 1999,
    "files": ["/images/KIT/KIT_9_1.jpeg", "/images/KIT/KIT_9_2.jpeg"]
  },
  {
    "id": 14,
    "title": "GRAB MY (ROSE)HIPS KIT",
    "category": "KIT",
    "price": 30,
    "mrp": 3259,
    "files": ["/images/KIT/KIT_10_1.jpeg", "/images/KIT/KIT_10_2.jpeg"]
  },
  {
    "id": 15,
    "title": "THREE WAY KIT", 
    "category": "KIT",
    "price": 42,
    "mrp": 5000,
    "files": ["/images/KIT/KIT_11_1.jpeg",
     "/images/KIT/KIT_11_2.jpeg"]
  },
  {
    "id": 16,
    "title": "EXFOLIATING SCALP SERUM",
    "category": "HAIR",
    "price": 17,
    "mrp": 2200,
    "files": ["/images/HAIR/HAIR_2_1.jpeg", "/images/HAIR/HAIR_2_2.jpeg"]
  },
  {
    "id": 17,
    "title": "CAFFEINATED SCALP SCRUB",
    "category": "HAIR",
    "price": 17,
    "mrp": 2340,
    "files": ["/images/HAIR/HAIR_3_1.jpeg",
     "/images/HAIR/HAIR_3_2.jpeg"]
  },
  {
    "id": 18,
    "title": "CAFFEINATED HAIR MASK",
    "category": "HAIR",
    "price": 67,
    "mrp": 6543,
    "files": ["/images/HAIR/HAIR_4_1.jpeg",
     "/images/HAIR/HAIR_4_2.jpeg"]
  },
  {
    "id": 19,
    "title": "DRY CLEAN VOLUME POWDER",
    "category": "HAIR",
    "price": 11,
    "mrp": 999,
    "files": ["/images/HAIR/HAIR_5_1.jpeg",
     "/images/HAIR/HAIR_5_2.jpeg"]
  },
  {
    "id": 20,
    "title": "SHIMMER LIP GLOSS",
    "category": "Lip Care",
    "price": 8,
    "mrp": 877,
    "files": ["/images/Lip_Care/LIP_1_1.jpeg",
     "/images/Lip_Care/LIP_1_2.jpeg"]
  },
  {
    "id": 21,
    "title": "CHERRY BOMB LIP SCRUB",
    "category": "Lip Care",
    "price": 8,
    "mrp": 1234,
    "files": ["/images/Lip_Care/LIP_2_1.jpeg",
     "/images/Lip_Care/LIP_2_2.jpeg"]
  },
  {
    "id": 22,
    "title": "TAUPE-LESS LIP & CHEEK TINT",
    "category": "Lip Care",
    "price": 7,
    "mrp": 823,
    "files": ["/images/Lip_Care/LIP_3_1.jpeg",
     "/images/Lip_Care/LIP_3_2.jpeg"]
  },
  {
    "id": 23,
    "title": "LIP BALM",
    "category": "Lip Care",
    "price": 299,
    "mrp": 399,
    "files": ["/images/Lip_Care/LIP_4_1.jpeg",
     "/images/Lip_Care/LIP_4_2.jpeg"]
  },
  {
    "id": 24,
    "title": "CHERRY BOMB LIP & CHEEK TINT",
    "category": "Lip Care",
    "price": 7,
    "mrp": 987,
    "files": ["/images/Lip_Care/LIP_5_1.jpeg",
     "/images/Lip_Care/LIP_5_2.jpeg"]
  },
  {
    "id": 25,
    "title": "LIP SCRUB",
    "category": "Body Care",
    "price": 4,
    "mrp": 399,
    "files": ["/images/Body_Care/BODY_1_1.jpeg", "/images/Body_Care/BODY_1_2.jpeg"]
  },
  {
    "id": 26,
    "title": "CHARCOAL FACE CLEANSER",
    "category": "Skin Care",
    "price": 10,
    "mrp": 1298,
    "files": ["/images/SKIN_CARE/SKIN_4_1.jpeg",
     "/images/SKIN_CARE/SKIN_4_2.jpeg"]
  },
  {
    "id": 27,
    "title": "THE ORIGINAL LIP DUO",
    "category": "Lip Care",
    "price": 20,
    "mrp": 1870,
    "files": ["/images/Lip_Care/LIP_6_1.jpeg",
     "/images/Lip_Care/LIP_6_2.jpeg"]
  },
  {
    "id": 28,
    "title": "GET ACTIVE FACE KIT",
    "category": "Skin Care",
    "price": 26,
    "mrp": 2987,
    "files": ["/images/SKIN_CARE/SKIN_5_1.jpeg",
     "/images/SKIN_CARE/SKIN_5_2.jpeg"]
  },
  {
    "id": 29,
    "title": "THE MEGA COMPLEXION CREW KIT",
    "category": "Skin Care",
    "price": 109,
    "mrp": 9999,
    "files": ["/images/SKIN_CARE/SKIN_6_1.jpeg",
     "/images/SKIN_CARE/SKIN_6_2.jpeg"]
  },
  {
    "id": 30,
    "title": "BRIGHTENING EYE SERUM",
    "category": "Skin Care",
    "price": 5,
    "mrp": 499,
    "files": ["/images/SKIN_CARE/SKIN_7_1.jpeg",
     "/images/SKIN_CARE/SKIN_7_2.jpeg"]
  },
  {
    "id": 31,
    "title": "CHARCOAL FACE MASK",
    "category": "Skin Care",
    "price": 6,
    "mrp": 876,
    "files": ["/images/SKIN_CARE/SKIN_8_1.jpeg",
     "/images/SKIN_CARE/SKIN_8_2.jpeg"]
  },
  {
    "id": 32,
    "title": "CAFFEINATED FACE MOISTURISER",
    "category": "Skin Care",
    "price": 17,
    "mrp": 1600,
    "files": ["/images/SKIN_CARE/SKIN_9_1.jpeg",
     "/images/SKIN_CARE/SKIN_9_1.jpeg"]
  },
  {
    "id": 33,
    "title": "ORIGINAL FACE SCRUB",
    "category": "Skin Care",
    "price": 9,
    "mrp": 823,
    "files": ["/images/SKIN_CARE/SKIN_10_1.jpeg",
     "/images/SKIN_CARE/SKIN_10_2.jpeg"]
  },
  {
    "id": 34,
    "title": "GLOW MASK",
    "category": "Skin Care",
    "price": 4,
    "mrp": 543,
    "files": ["/images/SKIN_CARE/SKIN_11_1.jpeg",
     "/images/SKIN_CARE/SKIN_11_2.jpeg"]
  },
  {
    "id": 35,
    "title": "BRIGHTENING VITAMIN C MASK",
    "category": "Skin Care",
    "price": 5,
    "mrp": 934,
    "files": ["/images/SKIN_CARE/SKIN_12_1.jpeg",
     "/images/SKIN_CARE/SKIN_12_1.jpeg"]
  },
  {
    "id": 36,
    "title": "EXTRA CLEAN FACE WASH",
    "category": "Body Care",
    "price": 11,
    "mrp": 988,
    "files": ["/images/Body_Care/BODY_2_1.jpeg",
     "/images/Body_Care/BODY_2_2.jpeg"]
  },
  {
    "id": 37,
    "title": "REWIND RETINOL SERUM",
    "category": "Body Care",
    "price": 8,
    "mrp": 987,
    "files": ["/images/Body_Care/BODY_3_1.jpeg",
     "/images/Body_Care/BODY_3_2.jpeg"]
  },
  {
    "id": 38,
    "title": "HYALURONIC ACID SERUM",
    "category": "Body Care",
    "price": 24,
    "mrp": 2300,
    "files": ["/images/Body_Care/BODY_4_1.jpeg",
     "/images/Body_Care/BODY_4_2.jpeg"]
  },
  {
    "id": 39,
    "title": "CERAMIDE NIGHT CREAM",
    "category": "Body Care",
    "price": 20,
    "mrp": 2344,
    "files": ["/images/Body_Care/BODY_5_1.jpeg",
     "/images/Body_Care/BODY_5_2.jpeg"]
  },
  {
    "id": 40,
    "title": "MARSHMALLOW LIP SCRUB",
    "category": "Body Care",
    "price": 6,
    "mrp": 986,
    "files": ["/images/Body_Care/BODY_6_1.jpeg",
     "/images/Body_Care/BODY_6_2.jpeg"]
  },
  {
    "id": 41,
    "title": "MARSHMALLOW LIP TINT",
    "category": "Lip Care",
    "price": 12,
    "mrp": 980,
    "files": ["/images/Lip_Care/LIP_7_1.jpeg",
     "/images/Lip_Care/LIP_7_2.jpeg"]
  },
  {
    "id": 42,
    "title": "CLEAN BODY WASH",
    "category": "EVERYDAY",
    "price": 10,
    "mrp": 1200,
    "files": ["/images/EVERYDAY/EVERYDAY_1_1.jpeg",
     "/images/EVERYDAY/EVERYDAY_1_2.jpeg"]
  },
  {
    "id": 43,
    "title": "CLEAN DEODORANT",
    "category": "EVERYDAY",
    "price": 234,
    "mrp": 321,
    "files": ["/images/EVERYDAY/EVERYDAY_2_1.jpeg",
     "/images/EVERYDAY/EVERYDAY_2_2.jpeg"]
  },
  {
    "id": 44,
    "title": "CLEAN BODY WASH: SUNDAY BRUNCH",
    "category": "EVERYDAY",
    "price": 11,
    "mrp": 1450,
    "files": ["/images/EVERYDAY/EVERYDAY_3_1.jpeg",
     "/images/EVERYDAY/EVERYDAY_3_2.jpeg"]
  },
  {
    "id": 45,
    "title": "CLEAN DEODORANT: CUCUMBER & GREEN TEA",
    "category": "EVERYDAY",
    "price": 10,
    "mrp": 924,
    "files": ["/images/EVERYDAY/EVERYDAY_4_1.jpeg",
     "/images/EVERYDAY/EVERYDAY_4_2.jpeg"]
  },
  {
    "id": 46,
    "title": "EXTRA CLEAN FACE WASH",
    "category": "EVERYDAY",
    "price": 12,
    "mrp": 999,
    "files": ["/images/EVERYDAY/EVERYDAY_5_1.jpeg",
     "/images/EVERYDAY/EVERYDAY_5_2.jpeg"]
  },
  {
    "id": 47,
    "title": "IN-SHOWER MOISTURISER",
    "category": "EVERYDAY",
    "price": 13,
    "mrp": 999,
    "files": ["/images/EVERYDAY/EVERYDAY_6_1.jpeg",
     "/images/EVERYDAY/EVERYDAY_6_2.jpeg"]
  },
  {
    "id": 48,
    "title": "Peptide Repair Rescue Micellar Shampoo",
    "category": "HAIR",
    "price": 9,
    "mrp": 1000,
    "files": ["/images/HAIR/HAIR_6_1.jpeg",
     "/images/HAIR/HAIR_6_2.jpeg"]
  },
  {
    "id": 49,
    "title": "Touch & Glow Powder",
    "category": "EVERYDAY",
    "price": 6,
    "mrp": 499,
    "files": ["/images/EVERYDAY/EVERYDAY_7_1.jpeg",
     "/images/EVERYDAY/EVERYDAY_7_2.jpeg"]
  },
  {
    "id": 50,
    "title": "Hair Accessory Set",
    "category": "HAIR",
    "price": 5,
    "mrp": 329,
    "files": ["/images/HAIR/HAIR_7_1.jpeg",
     "/images/HAIR/HAIR_7_2.jpeg"]
  },
  {
    "id": 51,
    "title": "Pink Shores 3-Wick Candle",
    "category": "Body Care",
    "price": 20,
    "mrp": 2799,
    "files": ["/images/Body_Care/BODY_7_1.jpeg",
     "/images/Body_Care/BODY_7_2.jpeg"]
  },
  {
    "id": 52,
    "title": "Carlton London Perfume Limited Edition Blush Perfume",
    "category": "Perfumes",
    "price": 10,
    "mrp": 2190,
    "files": ["/images/PERFUME/PERFUME_1_1.jpeg",
     "/images/PERFUME/PERFUME_1_2.jpeg"]
  },
  {
    "id": 53,
    "title": "Ramsons You are lovely Eau De Perfume",
    "category": "Perfumes",
    "price": 2,
    "mrp": 287,
    "files": ["/images/PERFUME/PERFUME_2_1.jpeg",
     "/images/PERFUME/PERFUME_2_2.jpeg"]
  },
  {
    "id": 54,
    "title": "Masaba By Nykaa Mini Pocket Perfume",
    "category": "Perfumes",
    "price": 2,
    "mrp": 299,
    "files": ["/images/PERFUME/PERFUME_3_1.jpeg",
     "/images/PERFUME/PERFUME_3_2.jpeg"]
  },
  {
    "id": 55,
    "title": "Chemist At Play Aqua Fragrance Under Arm Roll On For Lighter",
    "category": "Perfumes",
    "price": 4,
    "mrp": 399,
    "files": ["/images/PERFUME/PERFUME_4_1.jpeg",
     "/images/PERFUME/PERFUME_4_2.jpeg"]
  },
  {
    "id": 56,
    "title": "Moi By Nykaa Mini Pocket Perfume - Joie De Vivre",
    "category": "Perfumes",
    "price": 3,
    "mrp": 249,
    "files": ["/images/PERFUME/PERFUME_5_1.jpeg",
     "/images/PERFUME/PERFUME_5_2.jpeg"]
  },
  {
    "id": 57,
    "title": "Yves Saint Laurent Libre Eau De Parfum",
    "category": "perfumes",
    "price": 36,
    "mrp": 3100,
    "files": ["/images/PERFUME/PERFUME_6_1.jpeg",
     "/images/PERFUME/PERFUME_6_2.jpeg"]
  },
  {
    "id": 58,
    "title": "Ramsons Laopale Eau De Perfume",
    "category": "Perfumes",
    "price": 2,
    "mrp": 298,
    "files": ["/images/PERFUME/PERFUME_7_1.jpeg",
     "/images/PERFUME/PERFUME_7_2.jpeg"]
  },
  {
    "id": 59,
    "title": "Yves Saint Laurent Y Eau De Parfum",
    "category": "Perfumes",
    "price": 4200,
    "mrp": 6000,
    "files": ["/images/PERFUME/PERFUME_8_1.jpeg",
     "/images/PERFUME/PERFUME_8_2.jpeg"]
  },
  {
    "id": 60,
    "title": "Ramsons You are Sweet Eau De Perfume",
    "category": "Perfumes",
    "price": 165,
    "mrp": 299,
    "files": ["/images/PERFUME/PERFUME_9_1.jpeg",
     "/images/PERFUME/PERFUME_9_2.jpeg"]
  },
  {
    "id": 61,
    "title": "Bella Vita Organic Unisex Luxury Perfume Gift Set",
    "category": "Perfumes",
    "price": 594,
    "mrp": 849,
    "files": ["/images/PERFUME/PERFUME_10_1.jpeg",
     "/images/PERFUME/PERFUME_10_2.jpeg"]
  },
  {
    "id": 62,
    "title": "Nykaa Matte Luxe Lipstick - Artisanal Coffee",
    "category": "Lip Care",
    "price": 7,
    "mrp": 897,
    "files": ["/images/Lip_Care/LIP_8_1.jpg", "/images/Lip_Care/LIP_8_2.jpg"]
  },
  {
    "id": 63,
    "title": "Maybelline New York Sensational Liquid Matte",
    "category": "Lip Care",
    "price": 3,
    "mrp": 349,
    "files": ["/images/Lip_Care/LIP_9_1.jpg", "/images/Lip_Care/LIP_9_2.jpg "]
  },
  {
    "id": 64,
    "title": "Daily Life Forever52 Pro Artist Multitasker Lipstick Palette",
    "category": "Lip Care",
    "price": 28,
    "mrp": 2499,
    "files": ["/images/Lip_Care/LIP_10_1.jpg", "/images/Lip_Care/LIP_10_1.jpg"]
  },
  {
    "id": 65,
    "title": "Kay Beauty Matteinee Matte Lip Crayon Lipstick",
    "category": "Lip care",
    "price": 8,
    "mrp": 815,
    "files": ["/images/Lip_Care/LIP_11_1.jpg", "/images/Lip_Care/LIP_11_2.jpg"]
  },
  {
    "id": 66,
    "title": "Maybelline New York y Matte Lipstick",
    "category": "Lip care",
    "price": 3,
    "mrp": 325,
    "files": ["/images/Lip_Care/LIP_12_1.jpg", "/images/Lip_Care/LIP_12_2.jpg"]
  },
  {
    "id": 67,
    "title": "Lakme 9 To 5 Primer + Matte Lipstick",
    "category": "Lip care",
    "price": 5,
    "mrp": 526,
    "files": ["/images/Lip_Care/LIP_13_1.jpg", "root/images/Lip_Care/LIP_13_2.jpg"]
  },
  {
    "id": 68,
    "title": "Insight Cosmetics Non-Transfer Lip Color",
    "category": "Lip care",
    "price": 2,
    "mrp": 182,
    "files": ["/images/Lip_Care/LIP_14_1.jpg", "/images/Lip_Care/LIP_14_2.jpg"]
  },
  {
    "id": 69,
    "title": "Maybelline New York Color Sensational Ultimattes Lipstick ",
    "category": "Lip care",
    "price": 4,
    "mrp": 549,
    "files": ["/images/Lip_Care/LIP_15_1.jpg", "/images/Lip_Care/LIP_15_2.jpg"]
  },
  {
    "id": 70,
    "title": "Dot & Key Gloss Boss Tinted Lip Balm SPF 30 Vitamin C",
    "category": "Lip care",
    "price": 3,
    "mrp": 249,
    "files": ["/images/Lip_Care/LIP_16_1.jpg", "/images/Lip_Care/LIP_16_2.jpg"]
  },
  {
    "id": 71,
    "title": "Earth Rhythm Lip & Cheek Tint ",
    "category": "Lip care",
    "price": 4,
    "mrp": 499,
    "files": ["/images/Lip_Care/LIP_17_1.jpg", "/images/Lip_Care/LIP_17_2.jpg"]
  },
  {
    "id": 72,
    "title": "Huda Beauty Naughty Nude Eyeshadow Palette",
    "category": "Makeup",
    "price": 54,
    "mrp": 4500,
    "files": ["/images/Makeup/MAKEUP_1_1.jpg",
     "/images/Makeup/MAKEUP_1_2.jpg"]
  },
  {
    "id": 73,
    "title": "Insight Cosmetics Pro Concealer Palette",
    "category": "Makeup",
    "price": 2,
    "mrp": 199,
    "files": ["/images/Makeup/MAKEUP_2_1.jpg", "/images/Makeup/MAKEUP_2_2.jpg"]
  },
  {
    "id": 74,
    "title": "Maybelline New York Fit me Loose Finishing Powder",
    "category": "Makeup",
    "price": 8,
    "mrp": 695,
    "files": ["/images/Makeup/MAKEUP_3_1.jpg", "/images/Makeup/MAKEUP_3_2.jpg"]
  },
  {
    "id": 75,
    "title": "Swiss Beauty Ultimate Shadow Palette",
    "category": "Makeup",
    "price": 3,
    "mrp": 269,
    "files": ["/images/Makeup/MAKEUP_4_1.jpg", "/images/Makeup/MAKEUP_4_2.jpg"]
  },
  {
    "id": 76,
    "title": "Maybelline New York Fit Me Matte",
    "category": "Makeup",
    "price": 3,
    "mrp": 325,
    "files": ["/images/Makeup/MAKEUP_5_1.jpg", "/images/Makeup/MAKEUP_5_2.jpg"]
  },
  {
    "id": 77,
    "title": "M.A.C Studio Fix Powder Plus Foundation",
    "category": "Makeup",
    "price": 15,
    "mrp": 2345,
    "files": ["/images/Makeup/MAKEUP_6_1.jpg", "/images/Makeup/MAKEUP_6_2.jpg"]
  },
  {
    "id": 78,
    "title": "Maybelline New York Super Stay Matte Ink Liquid Lipstick",
    "category": "Makeup",
    "price": 7,
    "mrp": 699,
    "files": ["/images/Makeup/MAKEUP_7_1.jpg", "/images/Makeup/MAKEUP_7_2.jpg"]
  },
  {
    "id": 79,
    "title": "Kay Beauty Matte Blush - Deep Plum",
    "category": "Makeup",
    "price": 8,
    "mrp": 825,
    "files": ["/images/Makeup/MAKEUP_8_1.jpg", "/images/Makeup/MAKEUP_8_2.jpg"]
  }
]

const popular_food = [
    {
        title: "All",
        category: "All",
        image : ""

    },
     {
        title: "Makeup",
        category: "Makeup",
        image : "/images/makeup.jpg"
    },
    {
        title: "Lip Care",
        category: "Lip Care",
        image : "/images/lip_care.jpg"
    },
    {
        title: "Perfumes",
        category: "Perfumes",
        image : "/images/perfumes.jpg"
    },
]


export {products, popular_food};

